import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CurrentLecturePage } from './current-lecture.page';

describe('CurrentLecturePage', () => {
  let component: CurrentLecturePage;
  let fixture: ComponentFixture<CurrentLecturePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CurrentLecturePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CurrentLecturePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
