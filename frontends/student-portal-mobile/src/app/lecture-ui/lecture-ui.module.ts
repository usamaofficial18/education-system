import { NgModule } from "@angular/core";
import { LecturePageModule } from "./lecture/lecture.module";
import { CurrentLecturePageModule } from "./current-lecture/current-lecture.module";

@NgModule({
  declarations: [],
  imports: [LecturePageModule, CurrentLecturePageModule],
  exports: [LecturePageModule, CurrentLecturePageModule]
})
export class LectureUiModule {}
